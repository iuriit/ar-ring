/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using System.Collections;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class DefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
        #region PRIVATE_MEMBER_VARIABLES

        private TrackableBehaviour mTrackableBehaviour;

        #endregion // PRIVATE_MEMBER_VARIABLES



        #region UNTIY_MONOBEHAVIOUR_METHODS

        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
            StartCoroutine("AutoFocus");

        }

        IEnumerator AutoFocus()
        {
            yield return new WaitForSeconds(2);
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        public GameObject[] typeButton;

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
            if (gameObject.name == "0")
            {
                GlobalScript.trackingObjIndex = 0;
                GlobalScript.curSel = GlobalScript.braceletSel;
                typeButton[0].SetActive(true);
                Transform content = typeButton[0].transform.Find("ScrollView/Viewport/Content");
                foreach (Transform child in content)
                {
                    child.gameObject.SetActive(true);
                }
                Transform ts = typeButton[0].transform.Find("ScrollView");
                ScrollView sv = ts.GetComponent<ScrollView>();
                sv.isSelect = false;
                sv.OnScrollValueChanged();
            }
            else if (gameObject.name == "1")
            {
                GlobalScript.trackingObjIndex = 1;
                GlobalScript.curSel = GlobalScript.ringSel;
                typeButton[1].SetActive(true);
                Transform content = typeButton[1].transform.Find("ScrollView/Viewport/Content");
                foreach (Transform child in content)
                {
                    child.gameObject.SetActive(true);
                }
                Transform ts = typeButton[1].transform.Find("ScrollView");
                ScrollView sv = ts.GetComponent<ScrollView>();
                sv.isSelect = false;
                sv.OnScrollValueChanged();
            }

            transform.SendMessage("ShowHideHand", true);
            if (GlobalScript.pickToneState == true)
            {
                Show();
            }
            if (GlobalScript.curSel != -1)
                Show();
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            //if (GlobalScript.curSel != -1)
            //    ControlButton.curObj.SendMessage("ShowSelectedModel", GlobalScript.curSel);
        }

        private void OnTrackingLost()
        {
            typeButton[0].SetActive(false);
            typeButton[1].SetActive(false);
            if (GlobalScript.trackingObjIndex == 0)
                GlobalScript.braceletSel = GlobalScript.curSel;
            else if (GlobalScript.trackingObjIndex == 1)
                GlobalScript.ringSel = GlobalScript.curSel;
            GlobalScript.trackingObjIndex = -1;
            GlobalScript.curSel = -1;
            transform.SendMessage("HideSelectedModel");
            ControlButton.curObj = null;
            transform.SendMessage("ShowHideHand", false);
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            StartCoroutine("AutoFocus");
        }

        public void Show()
        {
            Debug.Log("Show");
            transform.SendMessage("Init");
            transform.SendMessage("ShowCurrentModel");
            ControlButton.curObj = transform.gameObject;
        }

        #endregion // PRIVATE_METHODS
    }
}
