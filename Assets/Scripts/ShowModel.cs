﻿using UnityEngine;
using System.Collections;

public class ShowModel : MonoBehaviour {
	public GameObject[] models;
	public int idx = 0;
	public int matIdx = 0;

	public GameObject handBlendObj;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Init(){
//		idx = -1;
	}

	public void ShowSelectedModel(bool direction){
		if (direction == true) {	//next
			idx++;
			if (idx >= models.Length)
				idx = 0;
		} else {					//prev
			idx--;
			if (idx < 0)
				idx = models.Length - 1;
		}

		for (int i = 0; i < models.Length; i++) {
			models [i].SetActive (false);
		}

		models [idx].SetActive (true);
	}

    public void ShowSelectedModel(int index)
    {
        idx = index;
        for (int i = 0; i < models.Length; i++)
            models[i].SetActive(false);
        models[idx].SetActive(true);
    }

    public void ShowCurrentModel(){
		for (int i = 0; i < models.Length; i++) {
			models [i].SetActive (false);
		}

		models [idx].SetActive (true);
	}

	public void HideSelectedModel(){
		for (int i = 0; i < models.Length; i++) {
			models [i].SetActive (false);
		}
	}

	public void ChangeColor(bool direction){
		if (direction == true) {	//next
			matIdx++;
			if (matIdx >= 3)
				matIdx = 0;
		} else {					//prev
			matIdx--;
			if (matIdx < 0)
				matIdx = 2;
		}

		models [idx].GetComponent<RingInfo> ().Change(matIdx);
	}

	public void ShowHideHand(bool isShow){
		handBlendObj.SetActive (isShow);
	}
}
