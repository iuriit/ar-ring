﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Loading : MonoBehaviour
{
    public GameObject canvas;
    public GameObject splashPanel;
    public GameObject loadingText;
    public Sprite[] splashImages;

    private float timer = 0f;
    private ScreenOrientation prevOrt = ScreenOrientation.Unknown;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > 1.5f && timer <= 2f)
            splashPanel.GetComponent<Image>().color = new Color(1, 1, 1, (2f - timer) * 2);
        else if (timer > 2f && timer <= 3f)
            loadingText.SetActive(true);
        else if (timer > 3f)
            Application.LoadLevel("AR");
            //UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("AR");

        if (Screen.orientation == ScreenOrientation.Landscape && prevOrt != ScreenOrientation.Landscape)
        {
            canvas.GetComponent<CanvasScaler>().referenceResolution = new Vector2(800, 480);
            splashPanel.GetComponent<Image>().sprite = splashImages[0];
        }
        if (Screen.orientation == ScreenOrientation.Portrait && prevOrt != ScreenOrientation.Portrait)
        {
            canvas.GetComponent<CanvasScaler>().referenceResolution = new Vector2(480, 800);
            splashPanel.GetComponent<Image>().sprite = splashImages[1];
        }
        prevOrt = Screen.orientation;
    }
}
