﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class AllGUi : MonoBehaviour {

    public GameObject canvas;
    public GameObject skinPanel;
    public GameObject optionPanel;
    public GameObject torchButton;
    public Sprite[] torchImage;

    private GameObject tmpObj = null;
    public GameObject[] trackingObjs;
    public GameObject[] toneButtons;

    private ScreenOrientation prevOrt = ScreenOrientation.Unknown;
    private bool isTorch = false;
    private int curTone = 0;

    // Use this for initialization
    void Start() {
        //		StartCoroutine (ShowPanel());
        isTorch = false;
        OnTorchButton();
    }

    // Update is called once per frame
    void Update() {
        if (Screen.orientation == ScreenOrientation.Landscape && prevOrt != ScreenOrientation.Landscape)
        {
            canvas.GetComponent<CanvasScaler>().referenceResolution = new Vector2(800, 480);
        }
        if (Screen.orientation == ScreenOrientation.Portrait && prevOrt != ScreenOrientation.Portrait)
        {
            canvas.GetComponent<CanvasScaler>().referenceResolution = new Vector2(480, 800);
        }
        prevOrt = Screen.orientation;
    }

    public void PressedToneButton(int cmd) {
        if(curTone == cmd)
        {
            skinPanel.SetActive(false);
            optionPanel.SetActive(true);
        }
        else
        {
            Transform ts;
            ts = toneButtons[curTone].transform.Find("Check");
            ts.gameObject.SetActive(false);
            ts = toneButtons[cmd].transform.Find("Check");
            ts.gameObject.SetActive(true);
            curTone = cmd;
            tmpObj = toneButtons[cmd];
            Color c = new Color(tmpObj.GetComponent<UnityEngine.UI.Image>().color.r,
                                    tmpObj.GetComponent<UnityEngine.UI.Image>().color.g,
                                    tmpObj.GetComponent<UnityEngine.UI.Image>().color.b,
                                    1);
            GameObject.Find("Global").GetComponent<GlobalScript>().ChangeToneColor(c);
        }
    }

    public void OnSelectButton(int cmd)
    {
        print("onSelect");
        Debug.Log(GlobalScript.trackingObjIndex);
        if (GlobalScript.trackingObjIndex == 0)
        {
            trackingObjs[0].GetComponent<Vuforia.DefaultTrackableEventHandler>().Show();
            if (ControlButton.curObj == null)
                return;
            if (GlobalScript.curSel == cmd)
                ControlButton.curObj.SendMessage("ChangeColor", true);
            else
            {
                GlobalScript.curSel = cmd;
                ControlButton.curObj.SendMessage("ShowSelectedModel", cmd);
            }
        }
        else if (GlobalScript.trackingObjIndex == 1)
        {
            trackingObjs[1].GetComponent<Vuforia.DefaultTrackableEventHandler>().Show();
            if (ControlButton.curObj == null)
                return;
            if (GlobalScript.curSel == cmd)
                ControlButton.curObj.SendMessage("ChangeColor", true);
            else
            {
                GlobalScript.curSel = cmd;
                ControlButton.curObj.SendMessage("ShowSelectedModel", cmd);
            }
        }
    }

    public void HidePanel() {
        //		GameObject.Find ("Camera/Panel").GetComponent<SpringPanel> ().target = new Vector3 (400,0,0);
        //		GameObject.Find("Camera/Panel").GetComponent<SpringPanel> ().enabled = true;
        GameObject.Find("Camera/Panel").GetComponent<TweenAlpha>().ResetToBeginning();
        GameObject.Find("Camera/Panel").GetComponent<TweenAlpha>().to = 0;
        GameObject.Find("Camera/Panel").GetComponent<TweenAlpha>().delay = 0;
        GameObject.Find("Camera/Panel").GetComponent<TweenAlpha>().Play();
    }

    //	IEnumerator ShowPanel(){
    //		yield return new WaitForSeconds (3);
    //		GameObject.Find("Camera/Panel").GetComponent<SpringPanel> ().enabled = true;
    //	}

    public void OnColorButton()
    {
        skinPanel.SetActive(true);
        optionPanel.SetActive(false);
    }

    public void OnTorchButton()
    {
        isTorch = !isTorch;
        CameraDevice.Instance.SetFlashTorchMode(isTorch);
        torchButton.GetComponent<UnityEngine.UI.Image>().sprite = torchImage[isTorch ? 1 : 0];
    }
}
